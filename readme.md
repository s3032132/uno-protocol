
# UNO Protocol version 1.2

Here you will find a description of the shared commands that our server-side applications should contain, in order for
them
to communicate with each other.

## Commands
| **Command**      | **Sender**      | **Receiver**                      | **Arguments**                                                                                                                                                                                                                                                                    | **Description**                                                                                                                                                                                |
|------------------|-----------------|-----------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `HI`             | Client          | Server                            | `displayName` - the name chosen by the client <br/> `functionalities: Functionality[]` - the functionalities that are enabled on the client (see Functionality enum) <br/> `protocolVersion: String` - the version of the implemented protocol (e.g. `protocolVersion: "7.8.9"`) | Makes the initial connection to the server.                                                                                                                                                    |
| `CONNECTED`      | Server          | Client                            |                                                                                                                                                                                                                                                                                  | Confirms that the initial connection has been made.                                                                                                                                            |
| `JOIN_GAME`      | Client          | Server                            | `gameSize: int` - size of the requested game (no less than 2 and no more than 10)                                                                                                                                                                                                | Requests a game of the given gameSize. Puts the player in a queue for that game size.                                                                                                          |
| `QUEUE_JOINED`   | Server          | All clients in queue              | `displayName: String` - the name of the player that joined the queue <br/> `playerNames: String[]` - the names of all players in the queue                                                                                                                                       | Confirms that the player has been put into the queue. Sends the `displayName` of the user that has joined the queue. Also sends the `playerNames` of the players in the queue for convenience. |
| `QUEUE_LEFT`     | Server          | All clients in the queue          | `displayName: String` - the user that left the queue <br/> `playerNames: String[]` - the names of all players in the queue (for convenience)                                                                                                                                     | Broadcasts that a client has left the queue (client closed the TCP connection).                                                                                                                |
| `GAME_STARTED`   | Server          | All clients in queue              | `functionalities: Functionality[]` - the functionalities that are at least supported by all participating clients                                                                                                                                                                | Broadcasts that a new game has started (the lobby was filled).                                                                                                                                 |
| `ROUND_STARTED`  | Server          | All clients in game, individually | `initialCards: CardObject[]` - the 7 initial cards for the client                                                                                                                                                                                                                | Broadcasts that a new round has started. Sends the initial 7 cards to each player individually.                                                                                                |
| `PLAY_CARD`      | Client          | Server                            | `card: CardObject` - the card that the player wants to play                                                                                                                                                                                                                      | Requests the server to play a card on behalf of the client. (Server should validate it!)                                                                                                       |
| `NEXT_TURN`      | Server          | All clients in game               | `playedCard: CardObject` - the card played by the previous player <br/> `displayName: String` - the name of the next player <br/> `prevPlayerCardCount: int` - the amount of cards that the previous player still has left                                                       | Broadcasts that a card has been played and that it is now `displayName`'s turn.                                                                                                                |
| `DRAW_CARD`      | Client          | Server                            |                                                                                                                                                                                                                                                                                  | Requests the server to draw a card on behalf of the client.                                                                                                                                    |
| `ISSUE_CARD`     | Server          | Client                            | `card: CardObject[]` - the cards that are given to the client                                                                                                                                                                                                                    | Gives one or more cards to a specific client.                                                                                                                                                  |
| `ROUND_FINISHED` | Server          | All clients in game               | `displayName: String` - the name of the winning player <br/> `winnerScore: int` - the score of the winning player                                                                                                                                                                | Broadcasts that the current round has finished.                                                                                                                                                |
| `GAME_FINISHED`  | Server          | All clients in game               | `scoreboard: ScoreboardObject` - a key-value set of each player with their correspondings core                                                                                                                                                                                   | Broadcasts that the game has finished.                                                                                                                                                         |
| `ERROR`          | Server          | Client                            | `code` - the error code, specified below <br/> `description` - a description to make the error more clear <br/> See the table of error codes.                                                                                                                                    | Informs the client that an error has occurred.                                                                                                                                                 |
| `CHAT`           | Client / Server | Server / Client                   | `message` <br/> `displayName` - the name of the user sending the message, <u>only required for server to client communication</u>.                                                                                                                                               | Send a message in the game's chat (if supported)                                                                                                                                               |

## Error Codes

| **Code**                  | **Description**                                                                                         |
|---------------------------|---------------------------------------------------------------------------------------------------------|
| ```invalid_move```        | The move is invalid.                                                                                    |
| ```name_taken```          | This name has already been taken by another player on the same server.                                  |
| ```invalid_name```        | Invalid name, character(s) are not part of UTF-8.                                                       |
| ```invalid_command```     | The command is invalid.                                                                                 |
| ```invalid_argument```    | The argument is invalid.                                                                                |
| ```player_disconnected``` | A player has disconnected from the game.                                                                |
| `incompatible_version`    | The version that the client is trying to use is not compatible with the protocol version of the server. |

## Card JSON Object

Each card object (`CardObject`) that is sent over the network, is composed of one `color` and one `value`.
Below you can find the possible values for the `color` and `value` keys of the `CardObject`. Please mind that the card
and scoreboard objects do not have an enum or anything, since they are just JSON objects.

<table>
<tr><th><code>color</code></th><th><code>value</code></th></tr>
<tr><td>

| **Color**    | **Encoding** |
|--------------|--------------|
| Red          | `R`          |
| Green        | `G`          |
| Blue         | `B`          |
| Yellow       | `Y`          |
| Wild (black) | `W`          |

</td><td>

| **Card Value**    | **Encoding**               |
|-------------------|----------------------------|
| 0-9               | ```ZERO, ONE, ..., NINE``` |
| Skip              | ```S```                    |
| Draw              | ```D```                    |
| Wildcard          | ```W```                    |
| Wildcard + draw 4 | ```F```                    |
| Reverse card      | ```R```                    |
</td></tr> </table>


### Examples of `CardObject`

Red card with value 6:

```json
{
  "color": "R",
  "value": "SIX"
}
```

Unplayed wildcard:

```json
{
  "color": "W",
  "value": "W"
}
```

Played wildcard (user selected color blue):

```json
{
  "color": "R",
  "value": "W"
}
```

**Please remember that these values are NOT of type String but of type CardColor and CardValue!**

Card object as `playedCard` argument for the `nextTurn` command, including other parameters/arguments:

```json
{
  "playedCard": {
    "color": "Y",
    "value": "THREE"
  },
  "displayName": "Tom",
  "prevPlayerCardCount": 5
}
```

## Scoreboard JSON Object

This object contains key-value pairs of a player's name (`displayName`) with their corresponding final score (`score`).

### Example of a `ScoreboardObject`

```json
{
  "player1": 395,
  "player2": 534,
  "player3": 0,
  "player4": 167
}
```

## Functionalities

When sending the `hi` command from the client to the server, you also send a list of functionalities that your client
supports as a parameter. Once the client has joined a game (`joinGame`) and there are enough players, the game will
start by broadcasting the `gameStarted` command to all clients in the lobby. This also contains a `functionalities`
parameter, with the intersecting parameters of <u>all</u> clients. For example:

> - Client 1: chat, teamplay, multigame, lobby <br/>
> - Client 2: chat, multigame, lobby <br/>
> - Client 3: seven_o, chat, multigame, lobby <br/>
>
> Here, the intersecting functionalities are chat, multigame and lobby. The GAME_STARTED command from the server will
> then be as follows:
> ```json
> {
>    "command": "GAME_STARTED",
>    "functionalities": "[CHAT, MULTI_GAME, LOBBY]"
> }
> ```
> This makes sure that all clients support all functionalities that a given game is using.


# Sending and receiving commands
Below, you can find some example code snippets to help you implement the org.json.JSONObject() class.
### Sending a command
```java
JSONObject jo = new JSONObject();
String msg = bufIn.readLine();
String[] messageSplit = msg.split(" ");

switch (messageSplit[0]) {
  case "join" -> {
    if(messageSplit.length == 2) {
      try {
        int gameSize = Integer.parseInt(messageSplit[1]);
        if(gameSize < 2 || gameSize > 10) throw new Exception();
        jo.put("command", Command.JOIN_GAME);
        jo.put("gameSize", gameSize);
        out.println(jo);
        // 'out' is a printWriter that writes to the outputStream of the client socket, i.e., it sends the JSON object to the server.
      } catch (Exception e) {
        System.out.println("Please enter a valid number of players that you want to play with.");
      }
    }
  }
  case "play" -> {
    // TODO: implement
  }
  // ...
}
```
### Receiving a command
```java
JSONObject data = new JSONObject(req); // req = in.readLine()
Command cmd = data.getEnum(Command.class, "command"); // Using the provided Command enum

switch (cmd) {
case JOIN_GAME -> {
  int gameSize = data.getInt("gameSize");
  // TODO: implement
  }
}
```
# How to propose a change

If you want to propose a change, please fork the main branch, make your changes, and create a
merge request. Please pick a clear merge request title, and describe why you propose the change(s) in the merge
request's description.

### How do I edit a Markdown (.md) file?

You can edit a Markdown file natively in GitLab's in-browser code editor. However, this editor is a bit clunky and the
preview window does not always work.

We recommend using a website like https://stackedit.io/, and copy-paste your changes to the in-browser
code editor from GitLab. Alternatively, you can use IntelliJ IDEA for editing Markdown files, with the built-in preview
window to see how it actually looks.