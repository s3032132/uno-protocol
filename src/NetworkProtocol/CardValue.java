package NetworkProtocol;

public enum CardValue {
  ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, S, D, W, F, R
}
