package NetworkProtocol;

public enum Functionality {
  CHAT, TEAM_PLAY, MULTI_GAME, LOBBY, PROGRESSIVE, JUMP_IN, SEVEN_O
}